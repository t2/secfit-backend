"""
Tests for the workouts application.
"""
from django.test import TestCase
from workouts.models import Workout, Exercise, ExerciseInstance, WorkoutFile
from users.models import User
from comments.models import Comment
from django.http import HttpRequest
from workouts.permissions import IsOwner, IsOwnerOfWorkout, IsCoachAndVisibleToCoach, IsCoachOfWorkoutAndVisibleToCoach, IsPublic, IsWorkoutPublic, IsReadOnly
import datetime
import pytz
from rest_framework.test import APIClient
import base64


class ObjectWithWorkout():

    def __init__(self, workout):
        self.workout = workout

# Create your tests here.


class WorkoutsPermissionTestCase(TestCase):

    def setUp(self):
        # CREATE TEST DATA
        user_1 = User.objects.create(id=1, username="user_1")
        user_2 = User.objects.create(id=2, username="user_2", coach=user_1)

        Workout.objects.create(id=1, name="trening_1", date=datetime.datetime(
            2021, 2, 24, 20, 8, 7, 127325, tzinfo=pytz.UTC), owner=user_1, visibility="PU")
        Workout.objects.create(id=2, name="trening_2", date=datetime.datetime(
            2021, 2, 24, 20, 8, 7, 127325, tzinfo=pytz.UTC), owner=user_2, visibility="CO")
        Workout.objects.create(id=3, name="trening_3", date=datetime.datetime(
            2021, 2, 24, 20, 8, 7, 127325, tzinfo=pytz.UTC), owner=user_2, visibility="PR")

        # CREATE DEFAULT HTTP REQUEST
        request = HttpRequest()
        request.method = "POST"
        request.user = user_1
        request.data = {
            "workout": "workout/1/foo"
        }
        self.request = request

        # DEFINE VIEW PARAMETER
        self.view = "view"

    def test_is_owner(self):
        # LOCAL SETUP
        workout = Workout.objects.get(id=1)
        # user_1 = User.objects.get(id=1)
        user_2 = User.objects.get(id=2)
        is_owner = IsOwner()

        # TEST
        self.assertTrue(is_owner.has_object_permission(
            self.request, self.view, workout))
        self.request.user = user_2
        self.assertFalse(is_owner.has_object_permission(
            self.request, self.view, workout))

    def test_is_owner_of_workout(self):
        user_1 = User.objects.get(id=1)
        user_2 = User.objects.get(id=2)
        request = self.request
        is_owner_of_workout = IsOwnerOfWorkout()
        workout = Workout.objects.get(id=1)
        object_with_workout = ObjectWithWorkout(workout)

        # TEST
        self.assertTrue(is_owner_of_workout.has_permission(
            self.request, self.view))
        request.user = user_2
        self.assertFalse(is_owner_of_workout.has_permission(
            self.request, self.view))

        self.assertFalse(is_owner_of_workout.has_object_permission(
            self.request, self.view, object_with_workout))
        request.user = user_1
        self.assertTrue(is_owner_of_workout.has_object_permission(
            self.request, self.view, object_with_workout))

    def test_is_owner_of_workout_no_workout(self):
        request = self.request
        request.data = {}
        is_owner_of_workout = IsOwnerOfWorkout()
        self.assertFalse(is_owner_of_workout.has_permission(
            self.request, self.view))

    def test_is_owner_of_workout_GET(self):
        user_2 = User.objects.get(id=2)
        request = self.request
        request.user = user_2
        request.method = "GET"
        is_owner_of_workout = IsOwnerOfWorkout()
        self.assertTrue(is_owner_of_workout.has_permission(
            self.request, self.view))

    def test_is_coach_and_visible_to_coach(self):
        is_coach_and_visible_to_coach = IsCoachAndVisibleToCoach()
        user_2 = User.objects.get(id=2)
        workout_1 = Workout.objects.get(id=1)
        workout_2 = Workout.objects.get(id=2)
        workout_3 = Workout.objects.get(id=3)
        request = self.request
        self.assertTrue(is_coach_and_visible_to_coach.has_object_permission(
            request, self.view, workout_2))
        self.assertFalse(is_coach_and_visible_to_coach.has_object_permission(
            request, self.view, workout_3))
        request.user = user_2
        self.assertFalse(is_coach_and_visible_to_coach.has_object_permission(
            request, self.view, workout_1))

    def test_is_coach_of_workout_and_visible_to_coach(self):
        # LOCAL SETUP
        is_coach_of_workout_and_visible_to_coach = IsCoachOfWorkoutAndVisibleToCoach()
        user_2 = User.objects.get(id=2)
        workout_1 = Workout.objects.get(id=1)
        workout_2 = Workout.objects.get(id=2)
        workout_3 = Workout.objects.get(id=3)
        object_with_workout_1 = ObjectWithWorkout(workout_1)
        object_with_workout_2 = ObjectWithWorkout(workout_2)
        object_with_workout_3 = ObjectWithWorkout(workout_3)
        request = self.request

        # TEST
        self.assertTrue(is_coach_of_workout_and_visible_to_coach.has_object_permission(
            request, self.view, object_with_workout_2))
        self.assertFalse(is_coach_of_workout_and_visible_to_coach.has_object_permission(
            request, self.view, object_with_workout_3))
        request.user = user_2
        self.assertFalse(is_coach_of_workout_and_visible_to_coach.has_object_permission(
            request, self.view, object_with_workout_1))

    def test_is_public(self):
        # LOCAL SETUP
        public_workout = Workout.objects.get(id=1)
        coach_workout = Workout.objects.get(id=2)
        private_workout = Workout.objects.get(id=3)
        is_public = IsPublic()

        # TEST
        self.assertTrue(is_public.has_object_permission(
            self.request, self.view, public_workout))
        self.assertFalse(is_public.has_object_permission(
            self.request, self.view, coach_workout))
        self.assertFalse(is_public.has_object_permission(
            self.request, self.view, private_workout))

    def test_is_workout_public(self):
        # LOCAL SETUP
        public_workout = Workout.objects.get(id=1)
        coach_workout = Workout.objects.get(id=2)
        private_workout = Workout.objects.get(id=3)
        public_object_with_workout = ObjectWithWorkout(public_workout)
        coach_object_with_workout = ObjectWithWorkout(coach_workout)
        private_object_with_workout = ObjectWithWorkout(private_workout)
        is_workout_public = IsWorkoutPublic()

        # TEST
        self.assertTrue(is_workout_public.has_object_permission(
            self.request, self.view, public_object_with_workout))
        self.assertFalse(is_workout_public.has_object_permission(
            self.request, self.view, coach_object_with_workout))
        self.assertFalse(is_workout_public.has_object_permission(
            self.request, self.view, private_object_with_workout))

    def test_is_readonly(self):
        # LOCAL SETUP
        request = self.request
        is_read_only = IsReadOnly()

        # TEST
        self.assertFalse(is_read_only.has_object_permission(
            request, self.view, request))
        request.method = "PUT"
        self.assertFalse(is_read_only.has_object_permission(
            request, self.view, request))
        request.method = "DELETE"
        self.assertFalse(is_read_only.has_object_permission(
            request, self.view, request))
        request.method = "GET"
        self.assertTrue(is_read_only.has_object_permission(
            request, self.view, request))
        request.method = "HEAD"
        self.assertTrue(is_read_only.has_object_permission(
            request, self.view, request))
        request.method = "OPTIONS"
        self.assertTrue(is_read_only.has_object_permission(
            request, self.view, request))


class LogNewWorkoutTestCase(TestCase):

    def setUp(self):
        # CREATE TEST DATA
        user = User.objects.create(id=1, username="1")
        exercise = Exercise.objects.create(id=1, name="1")

        # SET API AND REQUEST
        self.client = APIClient()
        self.client.force_authenticate(user=user)
        self.endpoint = '/api/workouts/'
        self.exercise_instance = {
            "exercise": "/api/exercises/{}/".format(exercise.id),
            "number": "10",
            "sets": "10"
        }
        self.request_body = {
            'name': 'name',
            'date': '2021-03-02T08:38:00.000Z',
            'notes': 'notes',
            'visibility': 'PU',
            'exercise_instances': [self.exercise_instance],
        }

    def test_unauthenticated(self):
        # LOCAL SETUP
        user = User.objects.get(id=1)
        self.client.force_authenticate(user=None)

        # TEST
        response = self.client.post(
            self.endpoint,
            self.request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.status_code, 401)
        self.client.force_authenticate(user=user)

    def test_correct_exercise(self):
        # TEST
        response = self.client.post(
            self.endpoint,
            self.request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.status_code, 201)

    def test_no_name(self):
        # LOCAL SETUP
        request_body = self.request_body.copy()
        request_body["name"] = ""

        # TEST
        response = self.client.post(
            self.endpoint,
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_no_date(self):
        # LOCAL SETUP
        request_body = self.request_body.copy()
        request_body["date"] = ""

        # TEST
        response = self.client.post(
            self.endpoint,
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_bad_date_format(self):
        # LOCAL SETUP
        request_body = self.request_body.copy()
        request_body["date"] = "2021-03-0208:38:00.000Z"

        # TEST
        response = self.client.post(
            self.endpoint,
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_no_notes(self):
        # LOCAL SETUP
        request_body = self.request_body.copy()
        request_body["notes"] = ""

        # TEST
        response = self.client.post(
            self.endpoint,
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_no_visibility(self):
        # LOCAL SETUP
        request_body = self.request_body.copy()
        request_body["visibility"] = ""

        # TEST
        response = self.client.post(
            self.endpoint,
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_visibility_not_valid(self):
        # LOCAL SETUP
        request_body = self.request_body.copy()
        request_body["visibility"] = "NV"

        # TEST
        response = self.client.post(
            self.endpoint,
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_no_exercise_instances(self):
        # LOCAL SETUP
        request_body = self.request_body.copy()
        request_body["exercise_instances"] = ""

        # TEST
        response = self.client.post(
            self.endpoint,
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_non_existent_exercise_instances(self):
        # LOCAL SETUP
        request_body = self.request_body.copy()
        exercise_instance = self.exercise_instance.copy()
        exercise_instance["exercise"] = "/api/exercises/a/"
        request_body["exercise_instances"] = exercise_instance

        # TEST
        response = self.client.post(
            self.endpoint,
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_0_number_exercise_instances(self):
        # LOCAL SETUP
        request_body = self.request_body.copy()
        exercise_instance = self.exercise_instance.copy()
        exercise_instance["number"] = "0"
        request_body["exercise_instances"] = exercise_instance

        # TEST
        response = self.client.post(
            self.endpoint,
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_0_sets_exercise_instances(self):
        # LOCAL SETUP
        request_body = self.request_body.copy()
        exercise_instance = self.exercise_instance.copy()
        exercise_instance["sets"] = "0"
        request_body["exercise_instances"] = exercise_instance

        # TEST
        response = self.client.post(
            self.endpoint,
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.status_code, 400)


class ViewWorkoutTestCase(TestCase):

    def setUp(self):
        # CREATE TEST DATA
        user_1 = User.objects.create(id=1, username="user_1")
        user_2 = User.objects.create(id=2, username="user_2", coach=user_1)

        exercise_1 = Exercise.objects.create(
            id=1, name="workout_1", description="description_1")
        exercise_2 = Exercise.objects.create(
            id=2, name="workout_2", description="description_2")
        exercise_3 = Exercise.objects.create(
            id=3, name="workout_3", description="description_3")

        workout_1 = Workout.objects.create(id=1, name="trening_1", date=datetime.datetime(
            2021, 2, 24, 20, 8, 7, 127325, tzinfo=pytz.UTC), owner=user_1, visibility="PU")
        workout_2 = Workout.objects.create(id=2, name="trening_2", date=datetime.datetime(
            2021, 2, 24, 20, 8, 7, 127325, tzinfo=pytz.UTC), owner=user_2, visibility="CO")
        workout_3 = Workout.objects.create(id=3, name="trening_3", date=datetime.datetime(
            2021, 2, 24, 20, 8, 7, 127325, tzinfo=pytz.UTC), owner=user_2, visibility="PR")
        Workout.objects.create(id=4, name="trening_4", date=datetime.datetime(
            2021, 2, 24, 20, 8, 7, 127325, tzinfo=pytz.UTC), owner=user_1, visibility="CO")

        Comment.objects.create(id=1, workout=workout_1,
                               content='comment_1', owner=user_1)
        Comment.objects.create(id=2, workout=workout_2,
                               content='comment_2', owner=user_2)
        Comment.objects.create(id=3, workout=workout_3,
                               content='comment_3', owner=user_2)

        WorkoutFile.objects.create(id=1, workout=workout_1, owner=user_1)
        WorkoutFile.objects.create(id=2, workout=workout_2, owner=user_2)
        WorkoutFile.objects.create(id=3, workout=workout_3, owner=user_2)

        ExerciseInstance.objects.create(
            workout=workout_1, exercise=exercise_1, sets=10, number=10)
        ExerciseInstance.objects.create(
            workout=workout_2, exercise=exercise_2, sets=10, number=10)
        ExerciseInstance.objects.create(
            workout=workout_3, exercise=exercise_3, sets=10, number=10)

        # SET UP API VARIABLES
        self.client = APIClient()
        self.endpoint = '/api/workouts/'
        self.endpoint_comments = '/api/comments/'
        self.endpoint_exercises = '/api/exercises/'

    def test_get_all_workouts(self):
        """Should return all workouts that the user has access to."""
        user = User.objects.get(id=2)
        self.client.force_authenticate(user=user)

        response_workout = self.client.get(self.endpoint, follow=True)
        self.assertEqual(response_workout.status_code, 200)
        self.assertEqual(response_workout.data["count"], 3)

    def test_get_own_workout(self):
        # LOCAL SETUP
        workout_id = 3
        user = User.objects.get(id=2)
        self.client.force_authenticate(user=user)
        workout = Workout.objects.get(id=workout_id)

        # TEST WORKOUT ACCESS
        response_workout = self.client.get("{0}{1}".format(
            self.endpoint, str(workout_id)), follow=True)
        self.assertEqual(response_workout.status_code, 200)
        self.assertEqual(response_workout.data['name'], workout.name)
        self.assertEqual(
            response_workout.data['visibility'], workout.visibility)

        # TEST WORKOUT FILE ACCESS
        workout_file = response_workout.data['files'][0]['url']
        response_file = self.client.get(workout_file, follow=True)
        self.assertEqual(response_file.status_code, 200)
        self.assertEqual(response_file.data['id'], workout_id)

        # TEST EXERCISE ACCESS
        exercise_instances = response_workout.data['exercise_instances']
        response_exercises = self.client.get(self.endpoint_exercises)
        # results = response_exercises.data['results']
        exercises = [exercise['instances'][0]
                     for exercise in response_exercises.data['results']]
        for exercise_instance in exercise_instances:
            self.assertTrue(exercise_instance['url'] in exercises)

        # TEST COMMENT ACCESS
        response_comments = self.client.get(
            self.endpoint_comments, follow=True)
        comments = response_comments.data['results']
        comment = next(
            (comment for comment in comments if comment['id'] == workout_id), None)
        self.assertTrue(comment)

    def test_coach_get_athletes_workout(self):
        # LOCAL SETUP
        workout_id = 2
        user = User.objects.get(id=1)
        self.client.force_authenticate(user=user)

        # TEST WORKOUT ACCESS
        workout = Workout.objects.get(id=workout_id)
        response_workout = self.client.get("{0}{1}".format(
            self.endpoint, str(workout_id)), follow=True)
        self.assertEqual(response_workout.status_code, 200)
        self.assertEqual(response_workout.data['name'], workout.name)
        self.assertEqual(
            response_workout.data['visibility'], workout.visibility)

        # TEST WORKOUT FILE ACCESS
        workout_file = response_workout.data['files'][0]['url']
        response_file = self.client.get(workout_file, follow=True)
        self.assertEqual(response_file.status_code, 200)
        self.assertEqual(response_file.data['id'], workout_id)

        # TEST EXERCISE ACCESS
        exercise_instances = response_workout.data['exercise_instances']
        response_exercises = self.client.get(self.endpoint_exercises)
        # results = response_exercises.data['results']
        exercises = [exercise['instances'][0]
                     for exercise in response_exercises.data['results']]
        for exercise_instance in exercise_instances:
            self.assertTrue(exercise_instance['url'] in exercises)

        # TEST COMMENT ACCESS
        response_comments = self.client.get(
            self.endpoint_comments, follow=True)
        comments = response_comments.data['results']
        comment = next(
            (comment for comment in comments if comment['id'] == workout_id), None)
        self.assertTrue(comment)

    def test_get_public_workout(self):
        # LOCAL SETUP
        workout_id = 1
        user = User.objects.get(id=2)
        self.client.force_authenticate(user=user)
        workout = Workout.objects.get(id=workout_id)

        # TEST WORKOUT ACCESS
        response_workout = self.client.get("{0}{1}".format(
            self.endpoint, str(workout_id)), follow=True)
        self.assertEqual(response_workout.status_code, 200)
        self.assertEqual(response_workout.data['name'], workout.name)
        self.assertEqual(
            response_workout.data['visibility'], workout.visibility)

        # TEST WORKOUT FILE ACCESS
        workout_file = response_workout.data['files'][0]['url']
        response_file = self.client.get(workout_file, follow=True)
        self.assertEqual(response_file.status_code, 200)
        self.assertEqual(response_file.data['id'], workout_id)

        # TEST EXERCISE ACCESS
        exercise_instances = response_workout.data['exercise_instances']
        response_exercises = self.client.get(self.endpoint_exercises)
        # results = response_exercises.data['results']
        exercises = [exercise['instances'][0]
                     for exercise in response_exercises.data['results']]
        for exercise_instance in exercise_instances:
            self.assertTrue(exercise_instance['url'] in exercises)

        # TEST COMMENT ACCESS
        response_comments = self.client.get(
            self.endpoint_comments, follow=True)
        comments = response_comments.data['results']
        comment = next(
            (comment for comment in comments if comment['id'] == workout_id), None)
        self.assertTrue(comment)

    def test_doesnt_get_private_workout(self):
        # LOCAL SETUP
        workout_id = 3
        user = User.objects.get(id=1)
        self.client.force_authenticate(user=user)
        # workout = Workout.objects.get(id=workout_id)

        # TEST WORKOUT ACCESS
        response = self.client.get("{0}{1}".format(
            self.endpoint, str(workout_id)), follow=True)
        self.assertEqual(response.status_code, 403)
        response_comments = self.client.get(
            self.endpoint_comments, follow=True)
        comments = response_comments.data['results']
        comment = next(
            (comment for comment in comments if comment['id'] == workout_id), None)
        self.assertFalse(comment)

    def test_doesnt_get_coach_workout(self):
        # LOCAL SETUP
        workout_id = 4
        user = User.objects.get(id=2)
        self.client.force_authenticate(user=user)

        # TEST WORKOUT ACCESS
        response = self.client.get("{0}{1}".format(
            self.endpoint, str(workout_id)), follow=True)
        self.assertEqual(response.status_code, 403)
        response_comments = self.client.get(
            self.endpoint_comments, follow=True)
        comments = response_comments.data['results']
        comment = next(
            (comment for comment in comments if comment['id'] == workout_id), None)
        self.assertFalse(comment)

    def test_get_own_workouts(self):
        user = User.objects.get(id=2)
        self.client.force_authenticate(user=user)
        # workout = Workout.objects.get(id=3)
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], 3)


class UpdateWorkoutTestCase(TestCase):

    def setUp(self):
        # CREATE TEST DATA

        user_1 = User.objects.create(id=1, username="user_1")

        exercise_1 = Exercise.objects.create(
            id=1, name="workout_1", description="description_1")
        Exercise.objects.create(
            id=2, name="workout_2", description="description_2")

        workout_1 = Workout.objects.create(id=1, name="trening_1", date=datetime.datetime(
            2021, 2, 24, 20, 8, 7, 127325, tzinfo=pytz.UTC), owner=user_1, visibility="PU")

        WorkoutFile.objects.create(id=1, workout=workout_1, owner=user_1)

        ExerciseInstance.objects.create(
            workout=workout_1, exercise=exercise_1, sets=10, number=10)

        # SET UP API VARIABLES
        self.client = APIClient()
        self.endpoint = '/api/workouts/'
        self.client.force_authenticate(user=user_1)

    def test_update_workout_info(self):
        workout_id = 1
        new_name = 'updated'
        new_date = '2021-04-02T08:38:00Z'
        new_note = 'new note'
        new_visibility = 'CO'
        request_body = {
            'name': new_name,
            'date': new_date,
            'notes': new_note,
            'visibility': new_visibility,
            'exercise_instances': [],
            'files': []
        }
        response = self.client.put(
            "{}{}/".format(self.endpoint, workout_id),
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(response.data["name"], new_name)
        self.assertEqual(response.data["date"], new_date)
        self.assertEqual(response.data["notes"], new_note)
        self.assertEqual(response.data["visibility"], new_visibility)

    def test_update_exercise_instances(self):
        # workout_1 = Workout.objects.get(id=1)
        exercise_2_id = 2
        workout_id = 1
        new_name = 'updated'
        new_date = '2021-04-02T08:38:00.000Z'
        new_note = 'new note'
        new_visibility = 'CO'
        new_number = 1
        new_sets = 1
        exercise_instance_1 = {
            "exercise": '/api/exercises/{}/'.format(exercise_2_id),
            'number': new_number,
            'sets': new_sets
        }
        request_body = {
            'name': new_name,
            'date': new_date,
            'notes': new_note,
            'visibility': new_visibility,
            'exercise_instances': [exercise_instance_1],
            'files': []
        }
        response = self.client.put(
            "{}{}/".format(self.endpoint, workout_id),
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(
            response.data["exercise_instances"][0]["number"], new_number)
        self.assertEqual(
            response.data["exercise_instances"][0]["sets"], new_sets)
        self.assertTrue(response.data["exercise_instances"][0]["exercise"].endswith(
            exercise_instance_1["exercise"]))

    def test_add_exercise_instance(self):
        # workout_1 = Workout.objects.get(id=1)
        exercise_1_id = 1
        exercise_2_id = 2
        workout_id = 1
        new_name = 'updated'
        new_date = '2021-04-02T08:38:00.000Z'
        new_note = 'new note'
        new_visibility = 'CO'
        exercise_instance_1 = {
            "exercise": '/api/exercises/{}/'.format(exercise_1_id),
            'number': 1,
            'sets': 1
        }
        exercise_instance_2 = {
            "exercise": '/api/exercises/{}/'.format(exercise_2_id),
            'number': 2,
            'sets': 2
        }
        request_body = {
            'name': new_name,
            'date': new_date,
            'notes': new_note,
            'visibility': new_visibility,
            'exercise_instances': [exercise_instance_1, exercise_instance_2],
            'files': []
        }
        response = self.client.put(
            "{}{}/".format(self.endpoint, workout_id),
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(len(response.data["exercise_instances"]), 2)

    def test_remove_exercise_instance(self):
        workout_id = 1
        new_name = 'updated'
        new_date = '2021-04-02T08:38:00.000Z'
        new_note = 'new note'
        new_visibility = 'CO'
        request_body = {
            'name': new_name,
            'date': new_date,
            'notes': new_note,
            'visibility': new_visibility,
            'exercise_instances': [],
            'files': []
        }
        response = self.client.put(
            "{}{}/".format(self.endpoint, workout_id),
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(len(response.data["exercise_instances"]), 0)

    def test_files(self):
        workout_id = 1
        new_name = 'updated'
        new_date = '2021-04-02T08:38:00.000Z'
        new_note = 'new note'
        new_visibility = 'CO'
        response = self.client.get(
            "{}{}/".format(self.endpoint, workout_id), follow=True)
        request_body = {
            'name': new_name,
            'date': new_date,
            'notes': new_note,
            'visibility': new_visibility,
            'exercise_instances': [],
            'files': []
        }
        response = self.client.put(
            "{}{}/".format(self.endpoint, workout_id),
            request_body,
            format="json",
            follow=True
        )
        self.assertEqual(len(response.data["files"]), 0)
