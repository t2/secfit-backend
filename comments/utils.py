class Utils ():

    def isPublicOrCoach(workout, owner, requestUser):
        return (
            workout.visibility == "PU"
            or owner == request.user
            or (workout.visibility == "CO" and owner.coach == requestUser)
            or workout.owner == requestUser
        )