"""Permissions for comments"""
from rest_framework import permissions
from comments.utils import Utils

class IsCommentVisibleToUser(permissions.BasePermission):
    """
    Custom permission to only allow a comment to be viewed
    if one of the following holds:
    - The comment is on a public visibility workout
    - The comment was written by the user
    - The comment is on a coach visibility workout and the user is the workout owner's coach
    - The comment is on a workout owned by the user
    """

    def has_object_permission(self, request, view, obj):
        """" Write permissions are only allowed to the owner. """
        return(Utils.isPublicOrCoach(obj.workout, obj.owner, request.user))
