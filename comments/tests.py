"""
Tests for the comments application.
"""
from django.test import TestCase
from comments.models import Comment
from users.models import User
from workouts.models import Workout
import datetime
import pytz
from rest_framework.test import APIClient
from django.http import HttpRequest


class CommentListTestCase(TestCase):

    def setUp(self):
        user_1 = User.objects.create(id=1, username="user_1")
        workout = Workout.objects.create(id=1, name="trening_1", date=datetime.datetime(
            2021, 2, 24, 20, 8, 7, 127325, tzinfo=pytz.UTC), owner=user_1, visibility="PU")
        comment_content_1 = 'Test comment 1'
        comment_content_2 = 'Test comment 2'
        Comment.objects.create(
            owner=user_1, workout=workout, content=comment_content_1)
        Comment.objects.create(
            owner=user_1, workout=workout, content=comment_content_2)

        self.client = APIClient()
        self.client.force_authenticate(user=user_1)
        self.endpoint = '/api/comments/'

    def test_get_comments(self):
        response = self.client.get(
            self.endpoint,
            follow=True
        )
        self.assertEqual(response.data["count"], 2)
        self.assertEqual(response.status_code, 200)
