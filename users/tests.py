import json
import base64
from rest_framework import serializers
from rest_framework.test import APIClient
from django.test import TestCase
from users.serializers import UserSerializer
from users.models import User

# Create your tests here.

class UserSerializerTestCase(TestCase):

    def test_equal_passwords(self):
        serializer = UserSerializer(data={'password': '1', 'password1': '1'})
        equal = "equal"
        self.assertEqual(serializer.validate_password(equal), equal)

    def test_unequal_passwords(self):
        serializer = UserSerializer(data={'password': '1', 'password1': '2'})
        equal = "equal"
        self.assertRaises(serializers.ValidationError, serializer.validate_password, equal)

    def test_no_passwords(self):
        serializer = UserSerializer(data={'password': '', 'password1': ''})
        equal = "equal"
        self.assertRaises(serializers.ValidationError, serializer.validate_password, equal)

    def test_create(self):
        username = "rob"
        email = "rob"
        password = "rob"
        phone_number = "rob"
        country = "rob"
        city = "rob"
        street_address = "rob"
        val_dict = {
            "username": username,
            "email": email,
            "password": password,
            "phone_number": phone_number,
            "country": country,
            "city": city,
            "street_address": street_address
            }
        serializer = UserSerializer(val_dict)
        response = serializer.create(val_dict)
        del val_dict["password"]
        response_dict = {
            "username": response.username,
            "email": response.email,
            "phone_number": response.phone_number,
            "country": response.country,
            "city": response.city,
            "street_address": response.street_address
            }
        self.assertEqual(val_dict, response_dict)

class UserSignUpTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.endpoint = '/api/users/'
        self.request_body = {
            'username': 'a',
            'email': 'a@b.com',
            'phone_number': '',
            'country':'',
            'city': '',
            'street_address': '',
            'password': 'a',
            'password1': 'a'
        }

    def test_correct(self):
        response = self.client.post(
            self.endpoint,
            self.request_body,
            follow=True
        )
        self.assertEqual(response.status_code, 201)

    def test_no_email(self):
        request_body = self.request_body.copy()
        request_body['email'] = ''
        response = self.client.post(
            self.endpoint,
            request_body,
            follow=True
        )
        self.assertEqual(response.status_code, 201)

    def test_no_alfa(self):
        request_body = self.request_body.copy()
        request_body['email'] = 'ab.com'
        response = self.client.post(
            self.endpoint,
            request_body,
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_not_valid_top_level_domain(self):
        request_body = self.request_body.copy()
        request_body['email'] = 'a@b.c'
        response = self.client.post(
            self.endpoint,
            request_body,
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_no_username(self):
        request_body = self.request_body.copy()
        request_body['username'] = ''
        response = self.client.post(
            self.endpoint,
            request_body,
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_long_username(self):
        request_body = self.request_body.copy()
        request_body['username'] = 'a'*200
        response = self.client.post(
            self.endpoint,
            request_body,
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_long_password(self):
        request_body = self.request_body.copy()
        request_body['password'] = 'a'*200
        response = self.client.post(
            self.endpoint,
            request_body,
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_unequal_passwords(self):
        request_body = self.request_body.copy()
        request_body['password1'] = 'unequal'
        response = self.client.post(
            self.endpoint,
            request_body,
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_no_password(self):
        request_body = self.request_body.copy()
        request_body['password'] = ''
        response = self.client.post(
            self.endpoint,
            request_body,
            follow=True
        )
        self.assertEqual(response.status_code, 400)

    def test_no_password_1(self):
        request_body = self.request_body.copy()
        request_body['password1'] = ''
        response = self.client.post(
            self.endpoint,
            request_body,
            follow=True
        )
        self.assertEqual(response.status_code, 400)


class GetUserTestCase(TestCase):

    def test_get_user(self):
        user = User.objects.create(
            id=1, username="user",
            birthday="1996-11-25",
            favorite_exercise="benk",
            bio="Elske brett"
            )
        client = APIClient()
        response = client.get('/api/users/{}'.format(user.id), follow=True)
        self.assertEqual(response.data['id'], user.id)
        self.assertEqual(response.data['username'], user.username)
        self.assertEqual(response.data['bio'], user.bio)
        self.assertEqual(response.data['favorite_exercise'], user.favorite_exercise)
        self.assertEqual(response.data['birthday'], user.birthday)

class UserRegister2WayTestCase(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.endpoint = '/api/users/'
        self.request_body = {
            'username': 'a',
            'email': 'a@b.com',
            'phone_number': '',
            'country':'',
            'city': '',
            'street_address': '',
            'password': 'a',
            'password1': 'a'
        }

    def test_username_password(self):
        request_body = self.request_body.copy()
        usernames = ['a', 'ab', 'a'*10+'b'*10, "c1", "d1*", "e1+", "f.@+-_", "../path"]
        passwords = ['c', 'cd', 'c'*10+'d'*10, "e2", "f2*", "g2+", "h.@+-_", "../path"]
        for username in usernames:
            for password in passwords:
                with self.subTest():
                    request_body['username'] = username
                    request_body['password'] = password
                    request_body['password1'] = password
                    response = self.client.post(
                        self.endpoint,
                        request_body,
                        follow=True
                    )
                    if(username in ['d1*', '../path']):
                        self.assertEqual(
                            response.status_code,
                            400,
                            '{} / {}'.format(username, password)
                            )
                    else:
                        self.assertEqual(
                            response.status_code,
                            201,
                            '{} / {}'.format(username, password)
                            )
                        User.objects.get(username=username).delete()

    def test_username_email(self):
        request_body = self.request_body.copy()
        usernames = [
            'a',
            'ab',
            'a'*10+'b'*10,
            "c1", "d1*",
            "e1+", "f.@+-_",
            "../path"
        ]
        emails = [
            'c@mail.com',
            'cd@mail.com',
            'c'*10+'d'*10+'@mail.com',
            "123",
            "**@**.***",
            "**@**.com",
            "f.@+-_.com",
            "fo+-_oo@foasd.com",
            "fo@fo_o.com",
            "fo@fo+o.com"
            ]
        not_allowed_emails = [
            "123",
            "**@**.***",
            "**@**.com",
            "f.@+-_.com",
            "fo@fo_o.com",
            "fo@fo+o.com"
            ]
        not_allowed_usernames = [
            'd1*',
            '../path'
            ]
        for username in usernames:
            for email in emails:
                with self.subTest():
                    request_body['username'] = username
                    request_body['email'] = email
                    response = self.client.post(
                        self.endpoint,
                        request_body,
                        follow=True
                    )
                    if(username in not_allowed_usernames or email in not_allowed_emails):
                        self.assertEqual(
                            response.status_code,
                            400,
                            '{} / {}'.format(username, email)
                            )
                    else:
                        self.assertEqual(
                            response.status_code,
                            201,
                            '{} / {}'.format(username, email)
                            )
                        User.objects.get(username=username).delete()

    def test_password_email(self):
        request_body = self.request_body.copy()
        passwords = ['c', 'cd', 'c'*10+'d'*10, "e2", "f2*", "g2+", "h.@+-_", "../path"]
        emails = [
            'c@mail.com',
            'cd@mail.com',
            'c'*10+'d'*10+'@mail.com',
            "123", "**@**.***",
            "**@**.com",
            "f.@+-_.com",
            "fo+-_oo@foasd.com",
            "fo@fo_o.com",
            "fo@fo+o.com"
            ]
        not_allowed_emails = [
            "123",
            "**@**.***",
            "**@**.com",
            "f.@+-_.com",
            "fo@fo_o.com",
            "fo@fo+o.com"
            ]
        for password in passwords:
            for email in emails:
                with self.subTest():
                    request_body['password'] = password
                    request_body['password1'] = password
                    request_body['email'] = email
                    response = self.client.post(
                        self.endpoint,
                        request_body,
                        follow=True
                    )
                    if email in not_allowed_emails:
                        self.assertEqual(
                            response.status_code,
                            400,
                            '{} / {}'.format(password, email)
                        )
                    else:
                        self.assertEqual(
                            response.status_code,
                            201,
                            '{} / {}'.format(password, email)
                        )
                        User.objects.get(username='a').delete()


class LogIn(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.endpoint = '/api/remember_me/'
        user = User.objects.create(id=1, username="user_1")
        self.client.force_authenticate(user=user)


    def test_get_remember_me(self):
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 200, '{}'.format(response))
        self.client.force_authenticate(user=None)
        response = self.client.get(self.endpoint)
        self.assertEqual(response.status_code, 403, '{}'.format(response))

    def test_token(self):
        request_body = {
                'username': 'a',
                'email': 'a@b.com',
                'phone_number': '',
                'country':'',
                'city': '',
                'street_address': '',
                'password': 'a',
                'password1': 'a'
            }
        response = self.client.post( '/api/users/', request_body, follow=True)
        self.assertEqual(response.status_code, 201)

        request_body = {
                'username': 'a',
                'password': 'a',
            }
        response = self.client.post('/api/token/', request_body, follow=True)
        refresh_token = response.json()['refresh']
        self.assertEqual(response.status_code, 200, '{}'.format(response))

        request_body = {
            'refresh' : refresh_token
        }
        response = self.client.post('/api/token/refresh/', request_body, follow=True)
        self.assertEqual(response.status_code, 200, '{}'.format(response))


        request_body = {
                'username': 'b',
                'password': 'a',
            }
        response = self.client.post('/api/token/', request_body, follow=True)
        self.assertEqual(response.status_code, 401, '{}'.format(response))